import React, { useContext } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Switch from '@material-ui/core/Switch';
import { Button } from '@material-ui/core';
// import { styled } from '@material-ui/core/styles';
// import { Button } from '@material-ui/core';
import axios from 'axios';
import UserContext from './UserContext';

export default function BasicTable() {
  /*
  const MyButtonEdit = styled(Button)({
    background: 'linear-gradient(to top right, #6bfea9 32.77%, #13707a 97.9%)',
    border: 0,
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    color: 'white',
    height: 48,
    padding: '0 30px',
  });

  const MyButtonDelete = styled(Button)({
    background:
      'linear-gradient(to top right, #fe6b8b 2.94%, rgb(255, 23, 0) 50.84%, #ff8e53)',
    border: 0,
    borderRadius: '100%',
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    color: 'white',
    height: 48,
    width: 48,
    padding: '0 30px',
  });
  */
  const userContext = useContext(UserContext);
  let count = 0;

  const columns = [
    { title: 'id' },
    { title: 'last name' },
    { title: 'first name' },
    { title: 'valid' },
  ];

  const handleDelete = (id) => {
    const index = userContext.rows.findIndex((element) => element.id === id);
    const url = `http://127.0.0.1:5000/api/v1/users/delete/${index}`;
    axios.delete(url).then((response) => {
      userContext.setRows(response.data.users);
    });
  };

  const handleChange = (id) => {
    const index = userContext.rows.findIndex((element) => element.id === id);
    /** ****API******** */
    const url = `http://127.0.0.1:5000/api/v1/users/edit/${index}`;
    axios
      .patch(url, { valid: !userContext.rows[index].valid })
      .then((response) => {
        userContext.setRows(response.data.users);
      });
  };

  const handleUpdate = (id) => {
    window.scrollTo(0, document.body.scrollHeight);
    userContext.setEdit({
      isEdit: true,
      id,
      onEdit: false,
    });
  };

  const incrementCount = () => {
    count += 1;
    return count;
  };

  return (
    <TableContainer component={Paper}>
      <Table aria-label="simple table">
        <TableHead>
          <TableRow>
            {columns.map((col) => {
              return (
                <TableCell key={incrementCount()} id="tableTitle" align="left">
                  {col.title}
                </TableCell>
              );
            })}
            <TableCell key={incrementCount()} id="tableUpdate" align="left">
              Edit
            </TableCell>
            <TableCell key={incrementCount()} id="tableDelete" align="left">
              Delete
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {userContext.rows.map((row) => (
            <TableRow key={row.id}>
              <TableCell component="th" scope="row" align="left">
                {row.id}
              </TableCell>
              <TableCell align="left">{row.lastName}</TableCell>
              <TableCell align="left">{row.firstName}</TableCell>
              <TableCell align="left">
                <Switch
                  checked={row.valid}
                  onChange={() => {
                    handleChange(row.id);
                  }}
                  color="primary"
                />
              </TableCell>
              <TableCell align="left">
                <Button
                  variant="contained"
                  onClick={() => {
                    handleUpdate(row.id);
                  }}
                >
                  Edit
                </Button>
              </TableCell>
              <TableCell align="left">
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={() => {
                    handleDelete(row.id);
                  }}
                >
                  X
                </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
