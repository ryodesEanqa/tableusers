import React, { useEffect, useState } from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import ReactDOM from 'react-dom';
import './index.css';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import axios from 'axios';
import FormUser from './FormUser';
import BasicTable from './BasicTable';
import UserContext from './UserContext';
import theme from './theme';

const App = () => {
  const [rows, setRows] = useState([]);

  const [edit, setEdit] = useState([{ isEdit: false, id: '', onEdit: false }]);

  const userContext = {
    rows,
    setRows,
    edit,
    setEdit,
  };

  useEffect(() => {
    const fetchData = async () => {
      const url = 'http://127.0.0.1:5000/api/v1/users/all';
      const rowsFlask = await axios.get(url);
      setRows(rowsFlask.data.users);
    };
    fetchData();
  }, []);

  return (
    <>
      <UserContext.Provider value={userContext}>
        <CssBaseline />
        <ThemeProvider theme={theme}>
          <Container>
            <h1>List Users</h1>
            <BasicTable />
            <FormUser />
          </Container>
        </ThemeProvider>
      </UserContext.Provider>
    </>
  );
};

//= ===========================
ReactDOM.render(<App />, document.getElementById('root'));
