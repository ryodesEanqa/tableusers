import React from 'react';

export default React.createContext({
  rows: [],
  setRows: () => {},
  edit: [],
  setEdit: () => {},
});
