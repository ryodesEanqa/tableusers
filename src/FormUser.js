import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import React from 'react';
import { Button } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import axios from 'axios';
import UserContext from './UserContext';

const style = (theme) => ({
  root: {
    display: 'flex',
  },
  paper: {
    padding: theme.spacing(3),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
  },
  title: {
    background: theme.palette.gradient1.main,
    color: theme.palette.gradient1.contrastText,
    borderRadius: theme.shape.borderRadius,
    margin: theme.spacing(2),
    padding: theme.spacing(1, 2),
  },
  description: {
    marginTop: theme.spacing(16),
  },
});

class FormUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      textFirst: '',
      textLast: '',
    };
  }

  componentDidUpdate() {
    this.onUpdate();
  }

  onUpdate = () => {
    const { edit } = this.context;
    const { rows } = this.context;
    const { setEdit } = this.context;
    if (edit.isEdit && !edit.onEdit) {
      const index = rows.findIndex((elemnt) => elemnt.id === edit.id);
      this.setState({
        textFirst: rows[index].firstName,
        textLast: rows[index].lastName,
      });
      setEdit({
        isEdit: true,
        id: edit.id,
        onEdit: true,
      });
    }
  };

  handleSubmit = (event) => {
    const { textLast, textFirst } = this.state;
    const { setRows } = this.context;
    event.preventDefault();
    const user = {
      id: Date.now(),
      lastName: textLast,
      firstName: textFirst,
      valid: false,
    };

    const url = 'http://127.0.0.1:5000/api/v1/users/all';
    axios.post(url, user).then((response) => {
      setRows(response.data.users);
    });
    /** Formatage des input */
    this.setState({ textFirst: '', textLast: '' });
  };

  handleChangeFirst = (event) => {
    const textFirst = event.target.value;
    this.setState({ textFirst });
  };

  handleChangeLast = (event) => {
    const textLast = event.target.value;
    this.setState({ textLast });
  };

  handleCancel = () => {
    const { setEdit } = this.context;
    setEdit({ idEdit: false, id: '', onEdit: false });
    this.setState({ textFirst: '', textLast: '' });
  };

  handleEdit = () => {
    const { rows } = this.context;
    const { edit } = this.context;
    const { textFirst } = this.state;
    const { textLast } = this.state;
    const { setRows } = this.context;
    const { setEdit } = this.context;

    const index = rows.findIndex((elemnt) => elemnt.id === edit.id);

    /** ********* API *********** */
    const url = `http://127.0.0.1:5000/api/v1/users/edit/${index}`;
    axios
      .put(url, {
        textFirst,
        textLast,
      })
      .then((response) => {
        setRows(response.data.users);
        setEdit({ idEdit: false, id: '', onEdit: false });
        this.setState({ textFirst: '', textLast: '' });
      });
  };

  render() {
    const { textFirst } = this.state;
    const { textLast } = this.state;
    const { edit } = this.context;
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <TextField
            value={textFirst}
            name="firstName"
            onChange={this.handleChangeFirst}
            label="First name"
          />
          <TextField
            value={textLast}
            name="lastName"
            onChange={this.handleChangeLast}
            label="Last name"
          />
          {edit.isEdit ? (
            <span>
              <Button
                id="buttonEdit"
                color="primary"
                variant="contained"
                onClick={this.handleEdit}
              >
                Edit
              </Button>
              <Button
                id="buttonEdit"
                color="secondary"
                variant="contained"
                onClick={this.handleCancel}
              >
                Cancel
              </Button>
            </span>
          ) : (
            <Button
              id="buttonSubmit"
              type="submit"
              color="primary"
              variant="contained"
            >
              valid
            </Button>
          )}
        </form>
      </div>
    );
  }
}

FormUser.contextType = UserContext;

FormUser.propTypes = {
  classes: PropTypes.shape({ root: PropTypes.string }).isRequired,
};

export default withStyles(style, { withTheme: true })(FormUser);
