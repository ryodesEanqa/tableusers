import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      dark: '#2E2E85',
      main: '#5A3CB8',
      light: '#8E7CD9',
    },
    secondary: {
      main: '#34B2D0',
      light: '#728DD5',
    },
    gradient1: {
      main: 'linear-gradient(45deg, #8E7CD9 30%, #5A3CB8 90%)',
      contrastText: '#fff',
    },
    gradient2: {
      main: 'linear-gradient(45deg, #34B2D0 30%, #728DD5 90%)',
      contrastText: '#fff',
    },
  },
  shape: {
    borderRadius: 32,
  },
});

export default theme;
